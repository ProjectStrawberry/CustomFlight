package com.gmail.jd4656.customflight;

import me.arcaniax.hdb.api.DatabaseLoadEvent;
import me.arcaniax.hdb.api.HeadDatabaseAPI;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;
import java.util.Map;

public class ItemTranslator implements Listener {
    Map<String, ItemStack> items = new HashMap<>();

    private CustomFlight plugin;

    ItemTranslator(CustomFlight plugin) {
        this.plugin = plugin;

        for (int i=0; i<10; i++) {
            ItemStack curItem = new ItemStack(Material.LIME_CONCRETE, 1);
            ItemMeta curMeta = curItem.getItemMeta();

            curMeta.setDisplayName(String.valueOf(i));
            curItem.setItemMeta(curMeta);

            curItem.setAmount(i);

            items.put(String.valueOf(i), curItem);
        }
    }

    ItemStack get(String value) {
        return items.get(value);
    }

    private ItemStack rename(String name, ItemStack item) {
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        item.setItemMeta(meta);
        return item;
    }

    private void add(String name, ItemStack item) {
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        item.setItemMeta(meta);

        items.put(ChatColor.stripColor(name).toLowerCase(), item);
    }

    @EventHandler
    public void onDatabaseLoad(DatabaseLoadEvent event){
        plugin.getLogger().info("Loading Head Database heads");
        HeadDatabaseAPI api = new HeadDatabaseAPI();
        try {
            this.add("0", api.getItemHead("202"));
            this.add("1", api.getItemHead("193"));
            this.add("2", api.getItemHead("194"));
            this.add("3", api.getItemHead("195"));
            this.add("4", api.getItemHead("196"));
            this.add("5", api.getItemHead("197"));
            this.add("6", api.getItemHead("198"));
            this.add("7", api.getItemHead("199"));
            this.add("8", api.getItemHead("200"));
            this.add("9", api.getItemHead("201"));
            this.add("Save", api.getItemHead("7826"));
            this.add("Back", api.getItemHead("7827"));

            plugin.getLogger().info("Sucessfully loaded Head Database heads");
        } catch (NullPointerException e) {
            plugin.getLogger().info("Error loading head: " + e);
        }
    }
}
