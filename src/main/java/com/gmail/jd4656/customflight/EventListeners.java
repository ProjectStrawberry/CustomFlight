package com.gmail.jd4656.customflight;

import net.minecraft.server.v1_16_R3.NBTTagCompound;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_16_R3.inventory.CraftItemStack;
import org.bukkit.entity.AreaEffectCloud;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Mob;
import org.bukkit.entity.Player;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.entity.Firework;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Collection;
import java.util.HashMap;

public class EventListeners implements Listener {
    private CustomFlight plugin;

    EventListeners(CustomFlight p) {
        plugin = p;
    }

    @SuppressWarnings("unused")
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();
        if (plugin.flyingPlayers.containsKey(uuid) && !plugin.lastAttacked.containsKey(uuid)) {
            long timeLeft = plugin.flyingPlayers.get(player.getUniqueId().toString()).get("duration");
            if (!plugin.config.getStringList("enabledWorlds").contains(player.getWorld().getName())) {
                player.sendMessage(ChatColor.GOLD + "You logged off with flight time active, but flight is not enabled in this world. You still have " + ChatColor.RED +
                        plugin.convertTime(timeLeft) + ChatColor.GOLD + " of flight time left.");
                return;
            }
            player.setAllowFlight(true);
            player.sendMessage(ChatColor.GOLD + "You logged off with flight time active. You still have " + ChatColor.RED +
                    plugin.convertTime(timeLeft) + ChatColor.GOLD + " of flight time left.");
        }
    }

    @SuppressWarnings("unused")
    @EventHandler
    public void onPlayerDamage(EntityDamageByEntityEvent event) {
        Entity entity = event.getEntity();
        Entity damager = event.getDamager();

        if (damager instanceof Arrow) {
            if (((Arrow) damager).getShooter() instanceof Player) {
                Player shooter = (Player) ((Arrow) damager).getShooter();
                if ((entity instanceof Player) && shooter.hasPermission("customflight.override.player")) return;
                if ((entity instanceof Mob) && shooter.hasPermission("customflight.override.mob")) return;
                plugin.lastAttacked.put(shooter.getUniqueId().toString(), System.currentTimeMillis());
                if (plugin.flyingPlayers.containsKey(shooter.getUniqueId().toString()) && shooter.getAllowFlight()) {
                    shooter.sendMessage(ChatColor.DARK_RED + "Your flight has been temporarily disabled due to being in combat.");
                    shooter.setAllowFlight(false);
                }
            }
            if (entity instanceof Player) {
                Player damagedPlayer = (Player) entity;
                if (((Arrow) damager).getShooter() instanceof Player && damagedPlayer.hasPermission("customflight.override.player")) return;
                if (((Arrow) damager).getShooter() instanceof Mob && damagedPlayer.hasPermission("customflight.override.mob")) return;
                plugin.lastAttacked.put(damagedPlayer.getUniqueId().toString(), System.currentTimeMillis());
                if (plugin.flyingPlayers.containsKey(damagedPlayer.getUniqueId().toString()) && damagedPlayer.getAllowFlight()) {
                    damagedPlayer.sendMessage(ChatColor.DARK_RED + "Your flight has been temporarily disabled due to being in combat.");
                    damagedPlayer.setAllowFlight(false);
                }
            }
        }

        if (damager instanceof ThrownPotion) {
            if (((ThrownPotion) damager).getShooter() instanceof Player) {
                Player shooter = (Player) ((ThrownPotion) damager).getShooter();
                if ((entity instanceof Player) && shooter.hasPermission("customflight.override.player")) return;
                if ((entity instanceof Mob) && shooter.hasPermission("customflight.override.mob")) return;
                plugin.lastAttacked.put(shooter.getUniqueId().toString(), System.currentTimeMillis());
                if (plugin.flyingPlayers.containsKey(shooter.getUniqueId().toString()) && shooter.getAllowFlight()) {
                    shooter.sendMessage(ChatColor.DARK_RED + "Your flight has been temporarily disabled due to being in combat.");
                    shooter.setAllowFlight(false);
                }
            }
            if (entity instanceof Player) {
                Player damagedPlayer = (Player) entity;
                if (((ThrownPotion) damager).getShooter() instanceof Player && damagedPlayer.hasPermission("customflight.override.player")) return;
                if (((ThrownPotion) damager).getShooter() instanceof Mob && damagedPlayer.hasPermission("customflight.override.mob")) return;
                plugin.lastAttacked.put(damagedPlayer.getUniqueId().toString(), System.currentTimeMillis());
                if (plugin.flyingPlayers.containsKey(damagedPlayer.getUniqueId().toString()) && damagedPlayer.getAllowFlight()) {
                    damagedPlayer.sendMessage(ChatColor.DARK_RED + "Your flight has been temporarily disabled due to being in combat.");
                    damagedPlayer.setAllowFlight(false);
                }
            }
        }

        if (damager instanceof AreaEffectCloud) {
            ProjectileSource projectile = ((AreaEffectCloud) damager).getSource();
            if (projectile instanceof Player) {
                Player shooter = (Player) projectile;
                if ((entity instanceof Player) && shooter.hasPermission("customflight.override.player")) return;
                if ((entity instanceof Mob) && shooter.hasPermission("customflight.override.mob")) return;
                plugin.lastAttacked.put(shooter.getUniqueId().toString(), System.currentTimeMillis());
                if (plugin.flyingPlayers.containsKey(shooter.getUniqueId().toString()) && shooter.getAllowFlight()) {
                    shooter.sendMessage(ChatColor.DARK_RED + "Your flight has been temporarily disabled due to being in combat.");
                    shooter.setAllowFlight(false);
                }
            }
            if (entity instanceof Player) {
                Player damagedPlayer = (Player) entity;
                if (projectile instanceof Player && damagedPlayer.hasPermission("customflight.override.player")) return;
                if (projectile instanceof Mob && damagedPlayer.hasPermission("customflight.override.mob")) return;
                plugin.lastAttacked.put(damagedPlayer.getUniqueId().toString(), System.currentTimeMillis());
                if (plugin.flyingPlayers.containsKey(damagedPlayer.getUniqueId().toString()) && damagedPlayer.getAllowFlight()) {
                    damagedPlayer.sendMessage(ChatColor.DARK_RED + "Your flight has been temporarily disabled due to being in combat.");
                    damagedPlayer.setAllowFlight(false);
                }
            }
        }

        if (damager instanceof Player && ((entity instanceof Player) || (entity instanceof Mob))) {
            Player attackingPlayer = (Player) damager;
            if (entity instanceof Player && attackingPlayer.hasPermission("customflight.override.player")) return;
            if (entity instanceof Mob && attackingPlayer.hasPermission("customflight.override.mob")) return;
            plugin.lastAttacked.put(attackingPlayer.getUniqueId().toString(), System.currentTimeMillis());
            if (plugin.flyingPlayers.containsKey(attackingPlayer.getUniqueId().toString()) && attackingPlayer.getAllowFlight()) {
                attackingPlayer.sendMessage(ChatColor.DARK_RED + "Your flight has been temporarily disabled due to being in combat.");
                attackingPlayer.setAllowFlight(false);
            }
        }

        if (entity instanceof Player) {
            Player player = (Player) entity;
            if ((damager instanceof Player) && player.hasPermission("customflight.override.player")) return;
            if ((damager instanceof Mob) && player.hasPermission("customflight.override.mob")) return;
            if ((damager instanceof Firework)) {
                Firework fw = (Firework) damager;
                if (fw.hasMetadata("nodamage")) return;
            }
            plugin.lastAttacked.put(player.getUniqueId().toString(), System.currentTimeMillis());
            if (plugin.flyingPlayers.containsKey(player.getUniqueId().toString()) && player.getAllowFlight()) {
                player.sendMessage(ChatColor.DARK_RED + "Your flight has been temporarily disabled due to being attacked.");
                player.setAllowFlight(false);
            }
        }
    }

    @SuppressWarnings("unused")
    @EventHandler
    public void onPlayerFall(EntityDamageEvent event) {
        if (!(event.getEntity() instanceof Player)) return;
        EntityDamageEvent.DamageCause cause = event.getCause();
        Player player = (Player) event.getEntity();
        String uuid = player.getUniqueId().toString();

        if (plugin.fallImmune.containsKey(uuid) && event.getCause().equals(EntityDamageEvent.DamageCause.FALL)) {
            event.setCancelled(true);
            plugin.fallImmune.remove(player.getUniqueId().toString());
            return;
        }

        if (cause.equals(EntityDamageEvent.DamageCause.BLOCK_EXPLOSION) || cause.equals(EntityDamageEvent.DamageCause.CONTACT)
        || cause.equals(EntityDamageEvent.DamageCause.DROWNING) || cause.equals(EntityDamageEvent.DamageCause.FALL)
        || cause.equals(EntityDamageEvent.DamageCause.FALLING_BLOCK) || cause.equals(EntityDamageEvent.DamageCause.FIRE)
        || cause.equals(EntityDamageEvent.DamageCause.FIRE_TICK) || cause.equals(EntityDamageEvent.DamageCause.HOT_FLOOR)
        || cause.equals(EntityDamageEvent.DamageCause.LAVA) || cause.equals(EntityDamageEvent.DamageCause.LIGHTNING)
        || cause.equals(EntityDamageEvent.DamageCause.STARVATION) || cause.equals(EntityDamageEvent.DamageCause.SUFFOCATION)
        || cause.equals(EntityDamageEvent.DamageCause.VOID)) {
            if (player.hasPermission("customflight.override.environmental")) return;
            plugin.lastAttacked.put(uuid, System.currentTimeMillis());
            if (plugin.flyingPlayers.containsKey(uuid) && player.getAllowFlight()) {
                player.sendMessage(ChatColor.DARK_RED + "Your flight has been temporarily disabled due to taking damage.");
                player.setAllowFlight(false);
            }
        }

        if (cause.equals(EntityDamageEvent.DamageCause.POISON)) {
            if (player.hasPermission("customflight.override.player")) return;
            plugin.lastAttacked.put(uuid, System.currentTimeMillis());
            if (plugin.flyingPlayers.containsKey(uuid) && player.getAllowFlight()) {
                player.sendMessage(ChatColor.DARK_RED + "Your flight has been temporarily disabled due to taking damage.");
                player.setAllowFlight(false);
            }
        }
    }

    @SuppressWarnings("unused")
    @EventHandler
     public void ProjectileLaunchEvent(ProjectileLaunchEvent event) {
        Entity entity = event.getEntity();
        if (entity instanceof ThrownPotion) {
            ThrownPotion potion = (ThrownPotion) entity;
            Collection<PotionEffect> effects = potion.getEffects();
            boolean isPoison = false;

            for (PotionEffect effect : effects) {
                if (effect.getType().equals(PotionEffectType.POISON)) {
                    isPoison = true;
                    break;
                }
            }

            if (!isPoison) return;
            if (potion.getShooter() instanceof Player) {
                Player shooter = (Player) potion.getShooter();
                if (shooter.hasPermission("customflight.override.player")) return;
                plugin.lastAttacked.put(shooter.getUniqueId().toString(), System.currentTimeMillis());
                if (plugin.flyingPlayers.containsKey(shooter.getUniqueId().toString()) && shooter.getAllowFlight()) {
                    shooter.sendMessage(ChatColor.DARK_RED + "Your flight has been temporarily disabled due to being in combat.");
                    shooter.setAllowFlight(false);
                }
            }
        }
     }

    @SuppressWarnings("unused")
    @EventHandler
    public void PlayerMoveEvent(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        final String uuid = player.getUniqueId().toString();
        Location loc = event.getTo().clone().subtract(0, 1, 0);

        if (!plugin.flyingPlayers.containsKey(uuid) && plugin.fallImmune.containsKey(uuid) && (!loc.getBlock().getType().equals(Material.AIR) && !loc.getBlock().getType().equals(Material.VOID_AIR))) {
            new BukkitRunnable() {
                public void run() {
                    plugin.fallImmune.remove(uuid);
                }
            }.runTaskLater(plugin, 20);
        }
    }

    @SuppressWarnings("unused")
    @EventHandler
    public void PlayerChangedWorld(PlayerChangedWorldEvent event) {
        Player player = event.getPlayer();
        String world = player.getWorld().getName();
        String uuid = player.getUniqueId().toString();

        if (plugin.flyingPlayers.containsKey(uuid) && player.getAllowFlight() && !plugin.config.getStringList("enabledWorlds").contains(world)) {
            player.setAllowFlight(false);
            player.sendMessage(ChatColor.DARK_RED + "Flying is not enabled in this world - your flight has been disabled.");
        }

        if (plugin.flyingPlayers.containsKey(uuid) && !player.getAllowFlight() && plugin.config.getStringList("enabledWorlds").contains(world)) {
            if (plugin.lastAttacked.containsKey(uuid)) {
                long pvpCd = plugin.flyingPlayers.get(uuid).get("cooldown");
                if (System.currentTimeMillis() < (plugin.lastAttacked.get(uuid) + pvpCd)) return;
            }
            new BukkitRunnable() {
                public void run() {
                    player.setAllowFlight(true);
                    player.sendMessage(ChatColor.GOLD + "Your flight has been enabled.");
                }
            }.runTaskLater(plugin, 20);
        }
    }

    @SuppressWarnings("unused")
    @EventHandler
    public void PlayerInteractEvent(PlayerInteractEvent event) {
        Action action = event.getAction();
        Player player = event.getPlayer();
        ItemStack item = event.getItem();
        String uuid = player.getUniqueId().toString();
        if (!action.equals(Action.RIGHT_CLICK_BLOCK) && !action.equals(Action.RIGHT_CLICK_AIR)) return;
        if (item == null) return;

        net.minecraft.server.v1_16_R3.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);
        NBTTagCompound itemCompound = (nmsItem.hasTag() ? nmsItem.getTag() : new NBTTagCompound());

        String isFuel = itemCompound.getString("cfItem");
        if (isFuel == null) return;
        if (!isFuel.equals("true")) return;
        String group = itemCompound.getString("cfGroup");

        if (!player.hasPermission("customflight.use") || !player.hasPermission("customflight.group." + group)) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this flight fuel.");
            event.setCancelled(true);
            return;
        }

        if (!plugin.fuel.containsKey(group)) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There is no longer a matching configuration for this flight fuel.");
            event.setCancelled(true);
            return;
        }

        FlightItem flightItem = plugin.fuel.get(group);

        long flightDuration = flightItem.getDuration() * 1000;
        long pvpCd = flightItem.getPvpCooldown() * 1000;

        if (!plugin.config.getStringList("enabledWorlds").contains(player.getWorld().getName())) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Flight fuel is not enabled in this world.");
            event.setCancelled(true);
            return;
        }

        if (plugin.lastAttacked.containsKey(uuid)) {
            if (System.currentTimeMillis() < (plugin.lastAttacked.get(uuid) + pvpCd)) {
                long secondsUntilCd = (plugin.lastAttacked.get(uuid) + pvpCd) - System.currentTimeMillis();
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You've been in combat, or have taken damage recently. " +
                        "You may not activate flight fuel for another " + plugin.convertTime(secondsUntilCd));
                event.setCancelled(true);
                return;
            }
            plugin.lastAttacked.remove(uuid);
        }

        if (plugin.flyingPlayers.containsKey(uuid)) {
            long curDuration = plugin.flyingPlayers.get(uuid).get("duration");
            long curCd = plugin.flyingPlayers.get(uuid).get("cooldown");

            plugin.flyingPlayers.get(uuid).replace("duration", curDuration + flightDuration);
            if (pvpCd < curCd) {
                plugin.flyingPlayers.get(uuid).replace("cooldown", pvpCd);
            }
            player.sendMessage(ChatColor.GOLD + "Your flight time has been increased to " + ChatColor.RED + plugin.convertTime(curDuration + flightDuration));
        } else {
            plugin.flyingPlayers.put(uuid, new HashMap<>());
            plugin.flyingPlayers.get(uuid).put("duration", flightDuration);
            plugin.flyingPlayers.get(uuid).put("cooldown", pvpCd);
            plugin.fallImmune.put(uuid, true);
            player.setAllowFlight(true);
            player.sendMessage(ChatColor.GOLD + "You have activated " + ChatColor.RED + plugin.convertTime(flightDuration) + ChatColor.GOLD + " of flight time.");
        }

        plugin.removeItems(player.getInventory(), item, 1);
    }
}
