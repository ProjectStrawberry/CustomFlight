package com.gmail.jd4656.customflight;

import com.gmail.jd4656.InventoryManager.InventoryClickHandler;
import com.gmail.jd4656.InventoryManager.InventoryManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.IOException;

public class NumberInventory {

    public EnterHandler onEnter;
    InventoryManager manager;
    CustomFlight plugin;

    NumberInventory(String title, String number, CustomFlight plugin) {
        this.plugin = plugin;
        manager = new InventoryManager(title + number, 27, plugin);

        manager.withItem(2, plugin.itemTranslator.get("1"));
        manager.withItem(3, plugin.itemTranslator.get("2"));
        manager.withItem(4, plugin.itemTranslator.get("3"));
        manager.withItem(5, plugin.itemTranslator.get("4"));
        manager.withItem(6, plugin.itemTranslator.get("5"));

        manager.withItem(11, plugin.itemTranslator.get("6"));
        manager.withItem(12, plugin.itemTranslator.get("7"));
        manager.withItem(13, plugin.itemTranslator.get("8"));
        manager.withItem(14, plugin.itemTranslator.get("9"));
        manager.withItem(15, plugin.itemTranslator.get("0"));

        manager.withItem(21, plugin.itemTranslator.get("back"));
        manager.withItem(23, plugin.itemTranslator.get("save"));


        manager.withEventHandler(new InventoryClickHandler() {
            @Override
            public void handle(InventoryClickEvent event) {
                Player player = (Player) event.getWhoClicked();
                ItemStack clicked = event.getCurrentItem();
                event.setCancelled(true);

                if (clicked == null || clicked.getType().equals(Material.AIR)) {
                    return;
                }

                ItemMeta meta = clicked.getItemMeta();

                if (meta.getDisplayName().toLowerCase().equals("back")) {
                    String newNumber = "";
                    if (number.length() > 0) newNumber = number.substring(0, number.length() - 1);
                    NumberInventory newInv = new NumberInventory(title, newNumber, plugin);
                    newInv.getManager().show(player);
                    return;
                }

                if (meta.getDisplayName().toLowerCase().equals("save")) {
                    if (number.equals("")) return;
                    if (plugin.flightItems.get(player.getUniqueId()).getDuration() == -1) {
                        try {
                            plugin.flightItems.get(player.getUniqueId()).setDuration(Integer.parseInt(number));
                        } catch (NumberFormatException ignored) {}
                        NumberInventory cooldownInv = new NumberInventory("PVP Cooldown: ", "", plugin);
                        cooldownInv.getManager().show(player);
                        return;
                    }

                    if (plugin.flightItems.get(player.getUniqueId()).getPvpCooldown() == -1) {
                        FlightItem flightItem = plugin.flightItems.get(player.getUniqueId());
                        plugin.flightItems.remove(player.getUniqueId());
                        try {
                            flightItem.setPvpCooldown(Integer.parseInt(number));
                        } catch (NumberFormatException ignored) {}

                        plugin.fuel.put(flightItem.getId(), flightItem);
                        try {
                            plugin.fuel.save();
                            player.sendMessage(ChatColor.GREEN + "Your flight fuel has been created.");
                        } catch (IOException e) {
                            player.sendMessage(ChatColor.DARK_RED + "Error saving flight fuel.");
                            e.printStackTrace();
                        }
                        player.closeInventory();
                    }
                    return;
                }

                NumberInventory newInv = new NumberInventory(title, number + meta.getDisplayName(), plugin);
                newInv.getManager().show(player);
            }
        });
    }

    InventoryManager getManager() {
        return this.manager;
    }

    static class EnterHandler {
        EnterHandler() {}
        void handle(String number) {}
    }
}
