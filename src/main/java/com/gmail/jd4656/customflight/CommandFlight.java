package com.gmail.jd4656.customflight;

import com.gmail.jd4656.InventoryManager.InventoryClickHandler;
import com.gmail.jd4656.InventoryManager.InventoryManager;
import net.minecraft.server.v1_16_R3.NBTTagCompound;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.craftbukkit.v1_16_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.StringUtil;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class CommandFlight implements TabExecutor {

    private CustomFlight plugin;

    CommandFlight(CustomFlight p) {
        plugin = p;
    }

    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (args.length < 1) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }
            Player player = (Player) sender;
            if (!player.hasPermission("customflight.check")) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (!plugin.flyingPlayers.containsKey(player.getUniqueId().toString())) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You have no flight time.");
                return true;
            }
            long timeLeft = plugin.flyingPlayers.get(player.getUniqueId().toString()).get("duration");
            player.sendMessage(ChatColor.GOLD + "You have " + ChatColor.RED + plugin.convertTime(timeLeft) + ChatColor.GOLD + " of flight time left.");
            return true;
        }

        if (args[0].equals("reload")) {
            if (!sender.hasPermission("customflight.reload")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }

            plugin.reloadConfig();
            plugin.config = plugin.getConfig();

            sender.sendMessage(ChatColor.DARK_GREEN + "Configuration reloaded.");
            return true;
        }
        
        if (args[0].equals("give")) {
            if (!sender.hasPermission("customflight.manage")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (args.length < 2) {
                sender.sendMessage(ChatColor.GOLD + "Usage: /customflight spawn <groupname> - Gives you the fuel item of the specified group.");
                return true;
            }
            String groupName = args[1];
            if (!plugin.fuel.containsKey(groupName)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That fuel group does not exist.");
                return true;
            }

            ItemStack fuelItem = plugin.fuel.get(groupName).getItem();

            int amount = 1;
            Player targetPlayer;

            if (args.length < 4) {
                if (!(sender instanceof Player)) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "To use this command from the console, please specify a target player and amount.");
                    return true;
                }
                targetPlayer = (Player) sender;
            } else {
                targetPlayer = Bukkit.getPlayer(args[2]);
                if (targetPlayer == null) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Player not found.");
                    return true;
                }
                try {
                    amount = Integer.parseInt(args[3]);
                } catch (NumberFormatException e) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid number.");
                    return true;
                }
            }

            fuelItem.setAmount(amount);

            if (plugin.freeSpace(targetPlayer.getInventory(), fuelItem) < amount) {
                targetPlayer.getWorld().dropItem(targetPlayer.getLocation().clone().add(0, 1, 0), fuelItem);
            } else {
                targetPlayer.getInventory().addItem(fuelItem);
            }

            targetPlayer.sendMessage(ChatColor.GOLD + "You've received " + ChatColor.RED + amount + " " + groupName + ChatColor.GOLD + (amount == 1 ? " fuel item." : " fuel items."));
            if (!targetPlayer.equals(sender)) {
                sender.sendMessage(ChatColor.GOLD + "You've given " + targetPlayer.getDisplayName() + " " + ChatColor.RED + amount + " " + groupName + ChatColor.GOLD + (amount == 1 ? " fuel item." : " fuel items."));
            }
            return true;
        }

        if (args[0].equals("remove")) {
            if (!sender.hasPermission("customflight.manage")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (args.length < 2) {
                sender.sendMessage(ChatColor.GOLD + "Usage: /flight remove <name> - Removes a flight fuel.");
                return true;
            }

            String groupName = args[1].toLowerCase();
            if (!plugin.fuel.containsKey(groupName)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "A fuel item with that name does not exist.");
                return true;
            }

            plugin.fuel.remove(groupName);

            try {
                plugin.fuel.save();
                sender.sendMessage(ChatColor.GOLD + "That flight fuel has been removed.");
            } catch (IOException e) {
                e.printStackTrace();
                sender.sendMessage(ChatColor.DARK_RED + "Error saving flight data.");
            }

            return true;
        }

        if (args[0].equals("create")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }
            if (!sender.hasPermission("customflight.manage")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (args.length < 2) {
                sender.sendMessage(ChatColor.GOLD + "Usage: /flight create <name> - Creates a new type of flight fuel.");
                return true;
            }

            Player player = (Player) sender;

            String groupName = args[1].toLowerCase();
            if (plugin.fuel.containsKey(groupName)) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "A fuel item with that name already exists.");
                return true;
            }


            ItemStack item = player.getInventory().getItemInMainHand();

            if (item.getType().equals(Material.AIR)) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You're not holding an item.");
                return true;
            }

            net.minecraft.server.v1_16_R3.ItemStack nmsItem = CraftItemStack.asNMSCopy(item);
            NBTTagCompound itemCompound = (nmsItem.hasTag() ? nmsItem.getTag() : new NBTTagCompound());

            itemCompound.setString("cfItem", "true");
            itemCompound.setString("cfGroup", groupName);

            nmsItem.setTag(itemCompound);

            FlightItem flightItem = new FlightItem();
            flightItem.setId(groupName);
            flightItem.setItem(CraftItemStack.asBukkitCopy(nmsItem));
            plugin.flightItems.put(player.getUniqueId(), flightItem);

            NumberInventory durationPicker = new NumberInventory("Duration: ", "", plugin);
            durationPicker.getManager().show(player);

            return true;
        }

        sender.sendMessage(ChatColor.DARK_AQUA + "Flight Commands:");
        sender.sendMessage(ChatColor.RED + "/flight " + ChatColor.GOLD + "- Shows how much flight time you have.");
        sender.sendMessage(ChatColor.RED + "/flight spawn <id> <player> <amount> " + ChatColor.GOLD + "- Gives you the specified fuel item for that group.");

        return true;
    }

    public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args) {
        List<String> tabComplete = new ArrayList<>();
        if (args.length == 1) {
            if (sender.hasPermission("customflight.manage")) {
                tabComplete.add("create");
                tabComplete.add("give");
                tabComplete.add("remove");
            }
            if (sender.hasPermission("customflight.reload")) tabComplete.add("reload");
        }
        if (args.length == 2 && args[0].equals("remove")  && sender.hasPermission("customtags.manage")) {
            tabComplete.addAll(plugin.fuel.getIds());
        }
        if (args.length == 2 && args[0].equals("give")  && sender.hasPermission("customtags.manage")) {
            tabComplete.addAll(plugin.fuel.getIds());
        }
        if (args.length == 3 && args[0].equals("give")  && sender.hasPermission("customtags.manage")) {
            tabComplete.add("<player>");
            tabComplete.addAll(Bukkit.getOnlinePlayers().stream().map(Player::getName).collect(Collectors.toList()));
        }
        if (args.length == 4 && args[0].equals("give")  && sender.hasPermission("customtags.manage")) {
            tabComplete.add("<amount>");
        }
        return (args.length > 0) ? StringUtil.copyPartialMatches(args[args.length - 1], tabComplete, new ArrayList<>()) : null;
    }
}
