package com.gmail.jd4656.customflight;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class FlightContainer {

    private CustomFlight plugin;
    Map<String, FlightItem> flightItems = new HashMap<>();

    File file;
    FileConfiguration config;

    FlightContainer(File file, CustomFlight p) {
        plugin = p;
        this.file = file;

        config = YamlConfiguration.loadConfiguration(file);

        if (config.contains("items")) {
            for (String key : config.getConfigurationSection("items").getKeys(false)) {
                flightItems.put(key, new FlightItem(config.getConfigurationSection("items").getConfigurationSection(key)));
            }
        }
    }

    FlightItem get(String key) {
        return this.flightItems.get(key);
    }

    boolean containsKey(String key) {
        return this.flightItems.containsKey(key);
    }

    void put(String key, FlightItem value) {
        this.flightItems.put(key, value);
    }

    void remove(String key) {
        flightItems.remove(key);
    }

    Set<String> getIds() {
        return this.flightItems.keySet();
    }

    void save() throws IOException {
        config.set("items", config.createSection("items"));
        for (FlightItem item : flightItems.values()) {
            plugin.getLogger().info("id: " + item.getId());
            config.getConfigurationSection("items").set(item.getId(), config.getConfigurationSection("items").createSection(item.getId()));
            config.getConfigurationSection("items." + item.getId()).set("id", item.getId());
            config.getConfigurationSection("items." + item.getId()).set("duration", item.getDuration());
            config.getConfigurationSection("items." + item.getId()).set("pvp-cooldown", item.getPvpCooldown());
            config.getConfigurationSection("items." + item.getId()).set("item", item.getItemString());
        }
        config.save(file);
    }
}
