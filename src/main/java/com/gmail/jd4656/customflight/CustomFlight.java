package com.gmail.jd4656.customflight;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.util.*;


public class CustomFlight extends JavaPlugin {
    private CustomFlight plugin;
    Map<String, Map<String, Long>> flyingPlayers = new HashMap<>();
    Map<String, Long> lastAttacked = new HashMap<>();
    Map<String, Boolean> fallImmune = new HashMap<>();

    FlightContainer fuel;
    FileConfiguration config;
    Map<UUID, FlightItem> flightItems = new HashMap<>();

    ItemTranslator itemTranslator;

    @Override
    public void onEnable() {
        plugin = this;
        plugin.getDataFolder().mkdirs();

        plugin.getConfig().options().copyDefaults(true);
        plugin.saveConfig();

        fuel = new FlightContainer(new File(this.getDataFolder() + "/items.yml"), this);


        this.config = plugin.getConfig();


        itemTranslator = new ItemTranslator(this);

        getServer().getPluginManager().registerEvents(itemTranslator, this);
        getServer().getPluginManager().registerEvents(new EventListeners(this), this);
        this.getCommand("flight").setExecutor(new CommandFlight(this));

        new BukkitRunnable() {
            public void run() {
                for (Map.Entry<String, Map<String, Long>> entry : flyingPlayers.entrySet()) {
                    String uuid = entry.getKey();
                    Map playerData = entry.getValue();
                    Player player = Bukkit.getPlayer(UUID.fromString(uuid));
                    long pvpCd = (long) playerData.get("cooldown");
                    long duration = (long) playerData.get("duration");

                    if (lastAttacked.containsKey(uuid)) {
                        if (System.currentTimeMillis() < (lastAttacked.get(uuid) + pvpCd)) {
                            continue;
                        }
                        fallImmune.put(uuid, true);
                        lastAttacked.remove(uuid);
                        if (player != null) {
                            if (!config.getStringList("enabledWorlds").contains(player.getWorld().getName())) {
                                player.sendMessage(ChatColor.DARK_RED + "Your flight would be enabled due to no longer being in combat, but flight is not enabled in this world.");
                                continue;
                            }
                            player.setAllowFlight(true);
                            player.sendMessage(ChatColor.GOLD + "Your flight is now enabled due to no longer being in combat.");
                        }
                    }

                    if (player == null || !player.getAllowFlight()) continue;

                    duration -= 1000;

                    flyingPlayers.get(uuid).put("duration", duration);

                    if (duration <= 0) {
                        player.setAllowFlight(false);
                        player.sendMessage(ChatColor.GOLD + "Your flight time has run out.");
                        flyingPlayers.remove(uuid);
                        continue;
                    }

                    if (duration == (5 * 60 * 1000)) {
                        player.sendMessage(ChatColor.RED + "Warning: " + ChatColor.DARK_RED + "You only have 5 minutes of flight time left.");
                        continue;
                    }
                    if (duration == (60 * 1000)) {
                        player.sendMessage(ChatColor.RED + "Warning: " + ChatColor.DARK_RED + "You only have 1 minute of flight time left.");
                        continue;
                    }
                    if (duration == (30 * 1000)) {
                        player.sendMessage(ChatColor.RED + "Warning: " + ChatColor.DARK_RED + "You only have 30 seconds of flight time left.");
                    }
                }
            }
        }.runTaskTimer(this, 0, 20);

        plugin.getLogger().info("CustomFlight loaded.");
    }

    String convertTime(long ms) {
        long seconds = (ms / 1000) % 60;
        long minutes = (ms / (1000 * 60)) % 60;
        long hours = (ms / (1000 * 60 * 60)) % 24;
        long days = (ms / (1000 * 60 * 60 * 24));

        String reply = "";
        if (days > 0) {
            reply += days + (days == 1 ? " day " : " days ");
        }
        if (hours > 0) {
            reply += hours + (hours == 1 ? " hour " : " hours ");
        }
        if (minutes > 0) {
            reply += minutes + (minutes == 1 ? " minute " : " minutes ");
        }
        if (seconds > 0) {
            reply += seconds + (seconds == 1 ? " second " : " seconds ");
        }
        if (ms < 1000) reply = "second";

        return reply.trim();
    }

    int countItems(Inventory inv, ItemStack item) {
        int count = 0;

        for (ItemStack curItem : inv) {
            if (curItem == null) continue;
            if (plugin.sameItem(curItem, item)) {
                count += curItem.getAmount();
            }
        }

        return count;
    }

    int freeSpace(Inventory inv, ItemStack item) {
        int free = 0;

        for (ItemStack curItem : inv.getStorageContents()) {
            if (curItem == null) {
                free += item.getMaxStackSize();
                continue;
            }
            if (!plugin.sameItem(curItem, item)) continue;
            free += (curItem.getMaxStackSize() - curItem.getAmount());
        }

        return free;
    }

    boolean sameItem(ItemStack item1, ItemStack item2) {
        ItemStack clonedItem1 = item1.clone();
        ItemStack clonedItem2 = item2.clone();
        clonedItem1.setAmount(1);
        clonedItem2.setAmount(1);

        if (clonedItem1.hasItemMeta() && clonedItem2.hasItemMeta()) {
            ItemMeta meta1 = clonedItem1.getItemMeta();
            ItemMeta meta2 = clonedItem2.getItemMeta();

            if ((meta1.hasItemFlag(ItemFlag.HIDE_POTION_EFFECTS) && meta1.hasDisplayName()) && (meta2.hasItemFlag(ItemFlag.HIDE_POTION_EFFECTS) && meta2.hasDisplayName())) {
                if (meta1.getDisplayName().equals("Indefinable Distillate") && meta2.getDisplayName().equals("Indefinable Distillate")) return true;
                if (meta1.getDisplayName().equals("Indefinable Brew") && meta2.getDisplayName().equals("Indefinable Brew")) return true;
            }
        }

        return (clonedItem1.hashCode() == clonedItem2.hashCode());
    }

    List<ItemStack> removeItems(Inventory inv, ItemStack item, int amountToRemove) {
        ItemStack[] contents = inv.getContents();
        List<ItemStack> removedItems = new ArrayList<>();
        int curAmount = 0;
        int slot = 0;

        for (ItemStack curItem : contents) {
            slot++;
            if (curItem == null || !plugin.sameItem(item, curItem)) continue;
            if ((curItem.getAmount() + curAmount) <= amountToRemove) {
                curAmount += curItem.getAmount();
                if (inv instanceof PlayerInventory && slot > 36) {
                    inv.setItem((slot - 1), null);
                } else {
                    inv.removeItem(curItem);
                }
                removedItems.add(curItem);
            } else {
                int removeAmount = (amountToRemove - curAmount);
                int newSize = curItem.getAmount() - removeAmount;
                curItem.setAmount(newSize);
                ItemStack clonedItem = curItem.clone();
                clonedItem.setAmount(removeAmount);
                if (inv instanceof PlayerInventory && slot > 36) {
                    inv.setItem((slot - 1), clonedItem);
                }
                removedItems.add(clonedItem);
                curAmount += removeAmount;
            }

            if (curAmount == amountToRemove) break;
        }

        return removedItems;
    }
}
