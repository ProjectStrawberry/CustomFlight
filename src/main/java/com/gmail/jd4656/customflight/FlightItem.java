package com.gmail.jd4656.customflight;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class FlightItem {

    private int duration = -1;
    private int pvpCooldown = -1;
    private ItemStack item;
    private String id;

    FlightItem() {

    }

    FlightItem(ConfigurationSection config) {
        duration = config.getInt("duration");
        pvpCooldown = config.getInt("pvp-cooldown");
        id = config.getString("id");

        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(config.getString("item")));
            BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);

            item = (ItemStack) dataInput.readObject();
            dataInput.close();
        } catch (IOException | ClassNotFoundException e) {
            Bukkit.getLogger().info("Error loading flight item " + config.getCurrentPath());
            e.printStackTrace();
        }
    }

    void setPvpCooldown(int cooldown) {
        this.pvpCooldown = cooldown;
    }

    void setDuration(int duration) {
        this.duration = duration;
    }

    void setItem(ItemStack item) {
        this.item = item;
    }

    void setId(String id) {
        this.id = id;
    }

    String getId() {
        return this.id;
    }

    public int getDuration() {
        return duration;
    }

    public int getPvpCooldown() {
        return pvpCooldown;
    }

    ItemStack getItem() {
        return item.clone();
    }
    String getItemString() {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);

            dataOutput.writeObject(this.item);
            dataOutput.close();

            return Base64Coder.encodeLines(outputStream.toByteArray());
        } catch (IOException e) {
            return "";
        }
    }
}
